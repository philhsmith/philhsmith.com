SHELL := /bin/bash

.PHONY: all
all:
	./hugo

.PHONY: clean
clean:
	-rm -r public

.PHONY: docker-image
docker-image: Dockerfile .dockerignore
	docker build -t philhsmith/philhsmith.com:latest .
	docker push philhsmith/philhsmith.com:latest

.PHONY: server
server:
	./hugo server --buildDrafts --bind 0.0.0.0 --port 1313
