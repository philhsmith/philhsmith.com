---
title: "Panpsychism"
date: 2020-01-12T23:40:51Z
author: phs
avatar: /img/phs.jpg
tags:
  - philosophy
  - great-courses
---

Recently I finished [Prof. Patrick Grim's][prof-patrick-grim] "Great Courses"
class [_Mind-Body Philosophy_][mind-body-philosophy].  It sounds fun to see if
I can articulate my own position on the questions he expounds on in the course.

In short, I am a [computationalist][computationalism]: consciousness is a
pattern that may appear in physical processes. I expect it can be translated
into different physical processes. I think the definitional boundary separating
conscious processes from other processes is subjective, even if there are
common landmarks we generally agree are on one side or the other.

That seems all well and good. However I am not a hard
[materialist][materialism]: I do not believe that physicalism addresses the
[hard problem of consciousness][hard-problem]. I like to note this by the
absence of timelessness in my experience: the present moment seems to be
distinguished from the perceived past and future only because I am experiencing
it. Contrast this to a physical description or model of myself and my
surroundings that seems sound without reference to a specific "present" moment.
To see that such a model does not contain a "now", think about simulating it:
you would need to choose a moment on the timeline to start, to insert a "now".
Physical models don't seem to capture the notion of "present moment". Attempts
to shoe-horn it in (with e.g. consciousness particles or what have you) seem
simultaneously untestable and offensive to Occam's razor.

Subjective experience then seems to be unaccounted for. I am content saying it
is not found in any model of physical processes, only in the physical processes
themselves. Notice I am no longer talking about consciousness: the computation
and brain state is still susceptible to description. Only the subjective
experience, whose toe we can catch with the present moment, escapes.

Odd as it might sound, I find it tempting to say that the subjective experience
is not personal. It seems incommunicable, but what I mean is that there is no
aspect of myself in it: that's all over in the computation (incidentally I am
reminded of one of the objections to ["Cogito, ergo sum"][cogito].)

At this point, the idea of ["Atman"][advaita-vendanta] is starting to look
pretty good.  However, besides Prof.  Grim's touch on the topic I know nothing
about it (haw). My impression of Atman is that it is ineffable, primitive and
pervasive. It does not have a location because it is not a thing: it is an
aspect of reality.

And now we arrive at panpsychism, or at least what I understand panpsychism to
be.  Subjective experience, but not the computational content that gives rise
to the more visible parts of consciousness, is a fundamental aspect of reality.
Rocks have subjective experience. Rocks are not conscious, unless they include
physical processes we choose to recognize as consciousness. A rock's subjective
experience is a lot like your subjective experience of the IR spectrum,
assuming that you are like me and have no qualia affected by it.

[prof-patrick-grim]: http://www.pgrim.org/
[mind-body-philosophy]: https://www.thegreatcoursesplus.com/show/mind_body_philosophy
[computationalism]: https://en.wikipedia.org/wiki/Computational_theory_of_mind
[materialism]: https://en.wikipedia.org/wiki/Materialism
[hard-problem]: https://en.wikipedia.org/wiki/Hard_problem_of_consciousness
[cogito]: https://en.wikipedia.org/wiki/Cogito,_ergo_sum#Use_of_%22I%22
[advaita-vendanta]: https://en.wikipedia.org/wiki/Advaita_Vedanta
