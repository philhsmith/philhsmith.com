---
title: "Philip H. Smith"
date: 2023-08-15T00:00:00Z
aliases:
- resume.html
---
<article>

<style>

#diagram {
  font-size: 2em;
}

#diagram text {
  fill: white;
}

#diagram text.circular {
  text-anchor: middle;
}

#diagram g.label.experience {
  font-size: 1.5em;
}

#diagram g.label.competency {
  font-size: 1.5em;
}

#diagram g.label.skill {
  font-size: 0.8em;
}

ul.competencies {
  display: none;
}

.date-range, .location, .gpa {
  float: right;
}

.details {
  padding-left: 2em;
  padding-right: 2em;
  text-align: justify;
}

h2 {
  margin-bottom: 0;
  font-size: 2.5em;
}

h3 {
  margin-top: 0;
  font-size: 1.5em;
}

</style>

<svg id="diagram"/>

<div id="prose">

<section class="experience">
  <h2>
    <a class="url" href="https://www.redfin.com/">
      <span class="organization" data-label="Redfin">Redfin</span>
    </a>
    <span class="date-range">
      <time datetime="2016-12" class="start">12/2016</time> -
      <time class="end current">Present</time>
    </span>
  </h2>

  <h3>
    <span class="role">Technical Lead</span>
    <span class="location">Seattle, USA</span>
  </h3>

  <div class="details">
    <p class="blurb">
      Guided policy and implemented tools and services over a long tenure
      usually relating to compute, virtualization or cloud infrastructure.
    </p>

    <p class="blurb">
      Led the adoption and deployment of Kubernetes, directly designing several
      key components of our integration.  In addition to championing the
      effort, I played a lead role in implementing the chosen security model,
      multi-region redundancy, integration with existing cloud and on-premise
      networks and several pieces of custom automation.
    </p>

    <p class="blurb">
      Since no off-the-shelf tool was available at the time, I created an
      Google OIDC client for kubectl, the kubernetes command line client.  This
      became how all Redfin engineers authenticated their tooling to the
      kubernetes clusters.  It used a variation of Google OAuth meant for smart
      TVs and other devices without an identified, integrated keyboard or
      browser.  Since our engineers used a variety of devices, OSes and
      browsers, this best matched our use case.
    </p>

    <p class="blurb">
      Several "buy" options for layering a deployment pipeline on top of
      vanilla kubernetes assume a static layout of "namespaces". Namespaces are
      a central feature of kubernetes which control access to secrets and
      hardware resources: a static namespace layout introduces unnecessary
      friction when the applications within are more dynamic.
    </p>

    <p class="blurb">
      To create the automation enabling a more dynamic, flexible namespace
      model we chose to build several "controllers" (robots running within the
      clusters) using <a
        href="https://github.com/GoogleCloudPlatform/metacontroller">metacontroller</a>.
      These controllers validate and responded to certain requests by creating
      configured namespaces and turning control of them over to the requester.
      The right to make these requests is protected with the native
      authorization rules, enabling controlled, dynamic dispersal of cluster
      resources.
    </p>

    <p class="blurb">
      Member of a small tiger team responsible for "lifting and shifting" our
      aging monolith out of a legacy data center and into AWS.  Migrated
      components needed to be modified to seamlessly support both environments
      over a period of months.

      <p>
        Among others components I was responsible for migrating:
        <ul>
          <li>web crawler traffic while preserving mission-critical SEO health metrics</li>
          <li>solr indexes driving the flagship search bar</li>
          <li>the daily release train</li>
          <li>in-house git hosting and related gitops workflows</li>
        </ul>
      </p>

      <p>In each case above I found ways to fully migrate before the big day.</p>

      <p>
        After the big day, I led a data-driven capacity-planning reassessment
        of our new cloud presence based on real traffic.  I identified and
        implemented three separate opportunities that in total led to six-digit
        savings off our monthly cloud bill.  This also resulted in an
        architectural proposal describing roughly twenty specific, actionable
        ideas to take advantage of cloud features to improve our availability,
        performance, security and budget posture.  This road map is still being
        implemented, and in particular is expected to lead to further savings
        of a similar magnitude.
      </p>
    </p>
  </div>

  <ul class="competencies">
    <li data-name="Dev" data-weight="2">
      Development
      <ul class="skills">
        <li data-weight="4">Shell</li>
        <li data-weight="3">Python</li>
        <li data-weight="1">Java</li>
      </ul>
    </li>
    <li data-name="Ops" data-weight="3">
      Operations
      <ul class="skills">
        <li data-weight="4">Kubernetes</li>
        <li data-weight="4">AWS</li>
        <li data-weight="4">Linux</li>
        <li data-weight="3">Networking</li>
        <li data-weight="2">Docker</li>
        <li data-weight="1">Jenkins</li>
      </ul>
    </li>
  </ul>
</section>

<section class="experience">
  <h2>
    <a class="url" href="https://moz.com/">
      <span class="organization" data-label="Moz">Moz</span>
    </a>
    <span class="date-range">
      <time datetime="2010-01" class="start">1/2010</time> -
      <time datetime="2016-08" class="end">8/2016</time>
    </span>
  </h2>

  <h3>
    <span class="role">Senior Developer (Big Data)</span>
    <span class="location">Seattle, USA</span>
  </h3>

  <div class="details">
    <p class="blurb">
      Architected, scaled and maintained web-scale databases,
      including link indexes of the Internet.
    </p>

    <p class="blurb">
      One prominent index is a legacy application written in C++,
      covering the requirements of crawling, indexing and serving
      queries over a restful interface.  It worked, but it was
      highly coupled and had poor test coverage, making new
      feature addition difficult.
    </p>

    <p class="blurb">
      To address these issues I created <a
        href="https://gitlab.com/philhsmith/sauce">sauce</a>, a
      C++ <a
        href="https://en.wikipedia.org/wiki/Dependency_injection">
        dependency injection</a> framework (Google's <a
        href="https://github.com/google/fruit">fruit</a>, which I
      would recommend in a C++14 world, was not yet published.)
      Sauce effectively allows a developer to replace a
      component's dependencies with <a
        href="https://github.com/google/googletest/blob/master/googlemock/README.md">
        mocks</a> or stubs during test.  This in turn greatly
      increases the developer's ability to simultaenously achieve
      good coverage and speed up tests that do not need, e.g. the
      filesystem or network.
    </p>

    <p class="blurb">
      Before joining the Big Data team the focus of my work was
      the design and implementation of a campaign application,
      created in <a href="http://rubyonrails.org/">Ruby on
        Rails</a>.  In an effort to retain modularity the
      application was architected as a series of <a
        href="https://en.wikipedia.org/wiki/Representational_state_transfer">
        restful webservices</a>, each charged with a different
      feature of the user's data experience.  This strategy was
      successful, but implied a lot of boilerplate overhead as
      each new service exposed its particular assortment of
      database-backed resources.
    </p>

    <p class="blurb">
      To ease the burden of exposing new resources, I created a
      Ruby on Rails library called <a
        href="https://rubygems.org/gems/responder_controller">
        responder controller</a>.  It filled a feature gap in the
      then-new version 3 of the framework, by providing a concise
      <a
        href="https://en.wikipedia.org/wiki/Domain-specific_language">DSL</a>
      for articulating the relationships between restful
      resources and their database model peers.
    </p>
  </div>

  <ul class="competencies">
    <li data-name="Development" data-weight="3">
      Development
      <ul class="skills">
        <li data-weight="4">C++</li>
        <li data-weight="4">Hadoop</li>
        <li data-weight="3">Shell</li>
        <li data-weight="3">Ruby</li>
        <li data-weight="2">Rails</li>
        <li data-weight="1">Python</li>
      </ul>
    </li>
    <li data-name="Leadership" data-weight="3">
      Leadership
      <ul class="skills">
        <li data-weight="4">Architecture</li>
        <li data-weight="3">Mentorship</li>
        <li data-weight="3">Adoption</li>
        <li data-weight="2">Recruitment</li>
      </ul>
    </li>
    <li data-name="Operations" data-weight="2">
      Operations
      <ul class="skills">
        <li data-weight="5">Linux</li>
        <li data-weight="4">Provisioning</li>
        <li data-weight="3">Capacity Planning</li>
        <li data-weight="3">Recovery</li>
      </ul>
    </li>
    <li data-name="Data Science" data-weight="1">
      Data Science
      <ul class="skills">
        <li data-weight="3">Performance Scaling</li>
        <li data-weight="2">Feature Engineering</li>
        <li data-weight="1">Model Selection</li>
      </ul>
    </li>
  </ul>
</section>

<section class="experience">
  <h2>
    <span class="organization" data-label="TDS">
      Tech. Distribution Solutions
    </span>
    <span class="date-range">
      <time datetime="2009-02" class="start">2/2009</time> -
      <time datetime="2009-11" class="end">11/2009</time>
    </span>
  </h2>

  <h3>
    <span class="role">Developer</span>
    <span class="location">Seattle, USA (remote)</span>
  </h3>

  <div class="details">
    <p class="blurb">
      Sole developer and adminstrator for a startup in a niche
      market in retail electronics.  Implemented in Ruby on Rails
      version 2 in a classic <a
         href="https://en.wikipedia.org/wiki/Multitier_architecture#Three-tier_architecture">
         three tier model</a>.
    </p>
  </div>

  <ul class="competencies">
    <li data-name="Development" data-weight="3">
      Development
      <ul class="skills">
        <li data-weight="5">Rails</li>
        <li data-weight="3">HTML</li>
        <li data-weight="2">CSS</li>
        <li data-weight="2">Javascript</li>
      </ul>
    </li>
    <li data-name="Operations" data-weight="2">
      Operations
      <ul class="skills">
        <li data-weight="4">Provisioning</li>
        <li data-weight="3">Linux</li>
        <li data-weight="2">Capacity Planning</li>
      </ul>
    </li>
  </ul>
</section>

<section class="experience">
  <h2>
    <a class="url" href="https://nextcapital.com/">
      <span class="organization" data-label="Business Logic">
        Business Logic
      </span>
    </a>
    <span class="date-range">
      <time datetime="2005-12" class="start">12/2005</time> -
      <time datetime="2009-02" class="end">2/2009</time>
    </span>
  </h2>

  <h3>
    <span class="role">Developer</span>
    <span class="location">Chicago, USA</span>
  </h3>

  <div class="details">
    <p class="blurb">
      Implemented and maintained a retirement finance Monte Carlo
      simulator as a web service.
    </p>

    <p class="blurb">
      To enable domain experts to rapidly iterate and to retain
      <a href="http://www.netlib.org/blas/">BLAS</a>-powered
      performance, the matrix-smashing aspects were implemented
      in <a
        href="http://www.mathworks.com/products/matlab/">Matlab</a>.
      This work was exposed under a restful <a
        href="http://tomcat.apache.org/">Tomcat</a> service using
      a pool of Matlab processes.  Each was exposed to Java
      through a novel interface best described as something
      between a glorified pipe and a terminal emulator.
    </p>

    <p class="blurb">
      Business Logic is now NextCapital.
    </p>
  </div>

  <ul class="competencies">
    <li data-name="Development" data-weight="3">
      Development
      <ul class="skills">
        <li data-weight="5">Java</li>
        <li data-weight="3">Matlab</li>
      </ul>
    </li>
    <li data-name="Operations" data-weight="1">
      Operations
      <ul class="skills">
        <li data-weight="2">Provisioning</li>
        <li data-weight="1">Linux</li>
      </ul>
    </li>
    <li data-name="Leadership" data-weight="1">
      Leadership
      <ul class="skills">
        <li data-weight="3">Mentorship</li>
        <li data-weight="2">Recruitment</li>
      </ul>
    </li>
  </ul>
</section>

<section class="experience">
  <h2>
    <a class="url" href="http://www.xby2.com/">
      <span class="organization" data-id="xby2" data-label="X by 2">X by 2</span>
    </a>
    <span class="date-range">
      <time datetime="2004-04" class="start">5/2004</time> -
      <time datetime="2005-12" class="end">12/2005</time>
    </span>
  </h2>

  <h3>
    <span class="role">Consultant</span>
    <span class="location">Farmington Hills, MI USA</span>
  </h3>

  <div class="details">
    <p class="blurb">
      Designed and implemented web service features as a
      client-facing consultant in the insurance industry.
    </p>
  </div>

  <ul class="competencies">
    <li data-name="Development" data-weight="3">
      Development
      <ul class="skills">
        <li data-weight="3">Java</li>
        <li data-weight="2">Websphere</li>
      </ul>
    </li>
    <li data-name="Client Collaboration" data-weight="1">
      Client Collaboration
      <ul class="skills">
        <li data-weight="2">Engagement</li>
        <li data-weight="1">Screen Mocking</li>
        <li data-weight="1">UML</li>
      </ul>
    </li>
  </ul>
</section>

<section class="experience">
  <h2>
    <a class="url" href="http://www.sandia.gov/">
      <span class="organization" data-label="">
        Sandia National Labs
      </span>
    </a>
    <span class="date-range">
      <time datetime="2004-01" class="start">1/2004</time> -
      <time datetime="2004-05" class="end">5/2004</time>
    </span>
  </h2>

  <h3>
    <span class="role">Graduate Student</span>
    <span class="location">Albuquerque, USA</span>
  </h3>

  <div class="details">
    <p class="blurb">
      Researched possible L1/2 CPU cache optimizations driven by
      patterns in concurrent data access.
    </p>

    <p class="blurb">
      Heuristics for maximizing cache efficiency by controlling
      <a href="https://en.wikipedia.org/wiki/Processor_affinity"
        >CPU thread affinity</a> were explored.  I helped
      implement benchmark programs.
    </p>
  </div>

  <ul class="competencies">
    <li data-name="Computer Science" data-weight="1">
      Computer Science
      <ul class="skills">
        <li data-weight="1">Parallel Task Scheduling</li>
        <li data-weight="1">L1/2 Cache Efficiency</li>
      </ul>
    </li>
  </ul>
</section>

<section class="experience">
  <h2>
    <a class="url" href="http://www.rhventures.org/">
      <span class="organization" data-label="Ventures">
        Rose-Hulman Ventures
      </span>
    </a>
    <span class="date-range">
      <time datetime="2001-09" class="start">9/2001</time> -
      <time datetime="2003-05" class="end">5/2003</time>
    </span>
  </h2>

  <h3>
    <span class="role">Student Developer</span>
    <span class="location">Terre Haute, IN USA</span>
  </h3>

  <div class="details">
    <p class="blurb">
      Contributed to design and implementation of software
      components in several incubating projects.
    </p>

    <p class="blurb">
      Student developers usually moved between incubating
      projects on a 3- or 6-month basis.  In this way I was
      exposed to and helped build several diverse projects:
    </p>

    <ul class="blurb">
      <li>A truck engine diagnostic tool that used sensor data to
        predict or diagnose failure</li>
      <li>A real estate web application featuring an (ad-hoc)
        <a href="https://en.wikipedia.org/wiki/Spatial_database#Spatial_index">
        spatial index</a></li>
      <li>A chemical engineering desktop <a
          href="http://opensoftwarefoundation.net/projects/lab2plant">application</a>
        to aid scaling chemical processes from laboratory to
        processing plant settings.</li>
      <li>A human-resources management <a
          href="http://www.peoplestrategy.com/">web
          application</a>.</li>
    </ul>
  </div>

  <ul class="competencies">
    <li data-name="Development" data-weight="3">
      Development
      <ul class="skills">
        <li data-weight="3">Java</li>
        <li data-weight="2">SQL</li>
        <li data-weight="1">HTML</li>
        <li data-weight="1">Javascript</li>
        <li data-weight="1">CSS</li>
      </ul>
    </li>
    <li data-name="Leadership" data-weight="2">
      Leadership
      <ul class="skills">
        <li data-weight="3">Mentorship</li>
        <li data-weight="2">Architecture</li>
      </ul>
    </li>
    <li data-name="Operations" data-weight="1">
      Operations
      <ul class="skills">
        <li data-weight="2">Tooling</li>
        <li data-weight="1">Linux</li>
      </ul>
    </li>
  </ul>
</section>

<hr/>

<section class="education">
  <h2>
    <a class="url" href="http://umich.edu/">
      <span class="organization" data-label="U Mich">
        University of Michigan
      </span>
    </a>
    <span class="date-range">
      <time class="start">2003</time> - <time class="end">2005</time>
    </span>
  </h2>

  <h3>
    <span class="degree">Master's in Computer Science</span>
    <span class="gpa">GPA 7.5 of 8</span>
  </h3>

  <div class="details">
    <p class="blurb">
      Emphasis on theory of computation, complexity theory and
      cryptography.  Advisor <a
       href="http://web.eecs.umich.edu/~kjc/">Kevin Compton</a>
    </p>
  </div>
</section>

<section class="education">
  <h2>
    <a class="url" href="http://www.rose-hulman.edu/">
      <span class="organization" data-label="Rose-Hulman">
        Rose-Hulman Institute of Tech.
      </span>
    </a>
    <span class="date-range">
      <time class="start">1999</time> - <time class="end">2003</time>
    </span>
  </h2>

  <h3>
    <span class="degree">Bachelor's in Math. and C.S.</span>
    <span class="gpa">GPA 3.6 of 4</span>
  </h3>

  <div class="details">
  </div>
</section>

</div>

</article>

<script src="https://cdnjs.cloudflare.com/ajax/libs/d3/4.2.2/d3.min.js"></script>

<script>
(function() {

d3.selectAll("time.current").attr("datetime",
  d3.timeFormat("%Y-%-m")(new Date()));

var experiences = d3.selectAll(".experience").nodes().map(
  function(i) {
    var e = d3.select(i);

    return {
      label: e.select(".organization").node().dataset.label,
      location: e.select(".location").text(),
      role: e.select(".role").text(),
      start: e.select(".start").attr("datetime"),
      end: e.select(".end").attr("datetime"),
      competencies: e.selectAll(".competencies > li").nodes().map(
        function(j) {
          var c = d3.select(j);

          return {
            name: j.dataset.name,
            weight: parseInt(j.dataset.weight, 10),
            skills: c.selectAll(".skills > li").nodes().map(
              function(k) {
                var s = d3.select(k);

                return {
                  name: s.text(),
                  weight: parseInt(k.dataset.weight, 10)
                };
              }
            )
          };
        }
      )
    };
  }
);

var resolution = 0.001;
var phase = (-9 / 32) * Math.PI;
var unit = [0, 1];

function line(r) {
  return d3.scaleLinear().domain(unit).range(r);
}

var angle = line([phase, 2 * Math.PI + phase]);
var centerRadius = line([150, 150]);
var experienceRadius = line([250, 175]);
var competencyRadius = line([350, 200]);
var maxSkillRadius = line([645, 225]);

function setSteps(ds, domain, range, preimage) {
  var scale =
    d3.scaleLinear().domain(domain(ds)).range(range(ds));

  ds.forEach(function(d, i) {
    var extent = preimage(d, i).map(scale).sort(function(a, b) {
      return a - b;
    });
    d.startStep = extent[0];
    d.endStep = extent[1];
  });
}

function parse(d) {
  return -d3.timeParse("%Y-%-m")(d);
}

setSteps(experiences,
  function(ds) {
    return d3.extent(
      ds.flatMap(function(d) {
        return [parse(d.start), parse(d.end)];
      }
    ));
  },
  function(ds) { return unit; },
  function(d) { return [d.start, d.end].map(parse); }
);

experiences.forEach(function(d, i) {
  setSteps(d.competencies,
    function(ds) {
      var max = ds.reduce(function(t, c) {
        c.start = t;
        c.end = t + c.weight;
        return c.end;
      }, 0);

      return [0, max];
    },
    function(ds) { return [d.startStep, d.endStep]; },
    function(c) { return [c.start, c.end]; }
  );

  d.experience = i;
  d.id = i.toString();
  d.position = 0;

  d.competencies.forEach(function(c, j) {
    setSteps(c.skills,
      function(ds) { return [0, ds.length]; },
      function(ds) { return [c.startStep, c.endStep]; },
      function(s, i) { return [i, i + 1]; }
    );

    c.experience = i;
    c.id = d.id + "-" + j;
    c.position = j / d.competencies.length;

    var max = d3.max(c.skills.map(function(s) {
      return s.weight;
    }));
    c.skills.forEach(function(s, k) {
      s.normalized = s.weight / max;
      s.experience = i;
      s.id = c.id + "-" + k;
      s.position = k / c.skills.length;
    });
  });
});

function area(inner, outer) {
  var area = d3.radialArea().angle(angle)
    .innerRadius(inner).outerRadius(outer);

  return function(d) {
    return area(d3.range(d.startStep, d.endStep, resolution));
  };
}

function skillRadius(d) {
  // no d3.interpolateFunction?
  var [c, s] = [(1 - d.normalized), d.normalized];
  return function(u) {
    return c * competencyRadius(u) + s * maxSkillRadius(u);
  };
}

var minTextEm = 0.01;
var textEmStep = 0.9;
var circularTextPad = 0.01;
var radialTextPad = 10;

function circularTextPath(inner, outer) {
  function middle(d) { return (inner(d) + outer(d)) / 2; }
  var arc = d3.radialLine().angle(angle).radius(middle);
  return function(d) {
    return arc(d3.range(
      d.startStep + circularTextPad,
      d.endStep - circularTextPad,
      resolution));
  };
}

function radialTextPath(inner, outer) {
  return function(d) {
    var middle = (d.startStep + d.endStep) / 2;
    var radial = d3.radialLine()
      .angle(angle(middle))
      .radius(line([
        inner(middle) + radialTextPad,
        outer(middle) - radialTextPad]
      ));
    return radial(d3.range(0, 1, resolution));
  };
}

function textPathRef(d) {
  return "#label-" + d.id;
}

function fitText(text) {
  var count = text.text().length;
  if (count == 0) {
    return;
  }

  var success = false;

  for (var em = 1; em >= minTextEm; em *= textEmStep) {
    text.style("font-size", "" + em + "em");

    // It fits if the last character renders
    try {
      var lastCharLength =
        text.node().getSubStringLength(count - 1, 1);
    } catch(e) {
      // (Some) renderers throw when asked for a clipped character
      continue;
    }

    if (lastCharLength > 0) {
      success = true;
      break;
    }
  }

  // webkit doesn't like dominant-baseline and this is easy
  text.attr("dy", "0.3em");

  if (success) {
    text.attr("visibility", "visible");
  } else {
    text.attr("visibility", "collapse");
  }
}

var fills = experiences.map(function(d, i) {
  var [a, b] = d3.schemeCategory20.slice(2 * i, 2 * (i + 1));
  return d3.interpolateRgb(a, b);
});

function fill(d) {
  // alert("d.experience " + d.experience);
  return fills[d.experience](d.position);
}

var svg = d3.select("#diagram")
  .attr("viewBox", "0 0 1000 1000");

var defs = svg.append("defs");

var content = svg.append("g")
  .attr("transform", "translate(500, 600)");

content.selectAll("path.area.experience")
  .data(experiences)
  .enter().append("path")
  .attr("class", "area experience")
  .style("fill", fill)
  .attr("d", area(centerRadius, experienceRadius));

defs.selectAll("path.text-path.experience")
  .data(experiences)
  .enter().append("path")
  .attr("class", "text-path experience")
  .attr("id", function(d) { return "label-" + d.id; })
  .attr("d", circularTextPath(centerRadius, experienceRadius));

content.selectAll("text.circular.experience")
  .data(experiences)
  .enter().append("g")
  .attr("class", "label circular experience")
  .append("text")
  .attr("class", "circular experience")
  .append("textPath")
  .attr("xlink:href", textPathRef)
  .attr("startOffset", "50%")
  .text(function(d) { return d.label; })
  .each(function(d) { fitText(d3.select(this.parentNode)); });

var competencies = experiences.flatMap(function(d) {
  return d.competencies;
});

content.selectAll("path.area.competency")
  .data(competencies)
  .enter().append("path")
  .attr("class", "area competency")
  .style("fill", fill)
  .attr("d", area(experienceRadius, competencyRadius));

defs.selectAll("path.text-path.competency")
  .data(competencies)
  .enter().append("path")
  .attr("class", "text-path competency")
  .attr("id", function(d) { return "label-" + d.id; })
  .attr("d",
    circularTextPath(experienceRadius, competencyRadius)
  );

content.selectAll("text.circular.competency")
  .data(competencies)
  .enter().append("g")
  .attr("class", "label circular competency")
  .append("text")
  .attr("class", "circular competency")
  .append("textPath")
  .attr("xlink:href", textPathRef)
  .attr("startOffset", "50%")
  .text(function(d) { return d.name; })
  .each(function(d) { fitText(d3.select(this.parentNode)); });

var skills = competencies.flatMap(function(c) {
  return c.skills;
});

content.selectAll("path.area.skill")
  .data(skills)
  .enter().append("path")
  .attr("class", "area skill")
  .style("fill", fill)
  .attr("d", function(d) {
    return area(competencyRadius, skillRadius(d))(d);
  });

defs.selectAll("path.text-path.skill")
  .data(skills)
  .enter().append("path")
  .attr("class", "text-path skill")
  .attr("id", function(d) { return "label-" + d.id; })
  .attr("d", function(d) {
    return radialTextPath(competencyRadius, skillRadius(d))(d);
  });

content.selectAll("text.radial.skill")
  .data(skills)
  .enter().append("g")
  .attr("class", "label radial skill")
  .append("text")
  .attr("class", "radial skill")
  .append("textPath")
  .attr("xlink:href", textPathRef)
  .text(function(d) { return d.name; })
  .each(function(d) { fitText(d3.select(this.parentNode)); });

})();
</script>
