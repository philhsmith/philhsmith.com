FROM alpine:latest
RUN apk add --no-cache bash curl git make \
    && U='https://github.com/gohugoio/hugo/releases' \
    && U="$U/download/v0.60.1/hugo_0.60.1_Linux-64bit.tar.gz" \
    && curl -sL "$U" | tar -C /usr/local/bin -zx hugo
